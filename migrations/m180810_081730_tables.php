<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180810_081730_tables
 */
class m180810_081730_tables extends Migration
{
    /**
     * {@inheritdoc}

    public function safeUp()
    {
    }

    /**
     * {@inheritdoc}

    public function safeDown()
    {
        echo "m180810_081730_tables cannot be reverted.\n";

        return false;
    }
	*/

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
    }

    public function down()
    {
        echo "m180810_081730_tables cannot be reverted.\n";

        return false;
    }
}
