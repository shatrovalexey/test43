<?php

namespace app\models;

/**
* Модель поездки
*/

class Trip extends \yii\db\ActiveRecord {
	/**
	* Имя таблицы
	*
	* @return string
	*/
	public static function tableName( ) {
		return '{{trip}}' ;
	}

	/**
	* Список поездок по названию аэропорта
	*
	* @param string $airport - название аэропорта
	*
	* @return \yii\db\ActiveRecord
	*/
	public static function findByAirportTitle( $airport ) {
		return self::find( )
			->select( '`cbt`.`trip`.*' )
			->innerJoin( '`cbt`.`trip_service`' , '`cbt`.`trip`.`id` = `cbt`.`trip_service`.`trip_id`' )
			->innerJoin( '`cbt`.`flight_segment`' , '`cbt`.`trip_service`.`id` = `cbt`.`flight_segment`.`flight_id`' )
			->innerJoin( '`nemo_guide_etalon`.`airport_name`' , '`nemo_guide_etalon`.`airport_name`.`airport_id` = `flight_segment`.`depAirportId`' )
			->where( [ '=' , '`cbt`.`trip`.`corporate_id`' , 3 ] )
			->andWhere( [ '=' , '`cbt`.`trip_service`.`service_id`' , 2 ] )
			->andWhere( [ '=' , '`nemo_guide_etalon`.`airport_name`.`value`' , $airport ] ) ;
	}
}