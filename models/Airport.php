<?php

namespace app\models;

/**
* Модель аэропорта
*/

class Airport extends \yii\db\ActiveRecord {
	/**
	* Имя таблицы
	*
	* @return string
	*/
	public static function tableName( ) {
		return '`nemo_guide_etalon`.`airport_name`' ;
	}

	/**
	* Список названий аэропортов
	*
	* @return \yii\db\ActiveRecord
	*/
	public static function getTitles( ) {
		return self::find( )
			->select( '`nemo_guide_etalon`.`airport_name`.`value` AS `title`' )
			->groupBy( [ '`nemo_guide_etalon`.`airport_name`.`value`' ] ) ;
	}
}