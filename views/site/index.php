<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
<form>
	<label>
		<input name="airport" required placeholder="airport" value="<?=htmlspecialchars( $airport )?>">
	</label>
	<label>
		<span>find</span>
		<input type="submit" value="&rarr;">
	</label>
</form>
<?=\yii\grid\GridView::widget( [
	'dataProvider' => $tripDataProvider ,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],

		'id' ,
		'corporate_id' ,
		'number' ,
		'user_id' ,
		'created_at' ,
		'updated_at' ,
		'coordination_at' ,
		'saved_at' ,
		'tag_le_id' ,
		'trip_purpose_id' ,
		'trip_purpose_parent_id' ,
		'trip_purpose_desc' ,
		'status' ,
	] ,
] ) ?>
</div>